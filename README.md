# Flux engine blog

Page: https://blog.fluxengine.ltd 

### Install Hugo as Your Site Generator (Binary Install)

Use the [installation instructions in the Hugo documentation](https://gohugo.io/getting-started/installing/).

```bash
./hugo server
```

